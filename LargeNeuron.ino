#include "LPD8806.h"
#include "SPI.h" 

// Number of RGB LEDs in strand:
int nLEDs = 20;
int nLEDs2 = 20;
int nLEDs3 = 48;

// Chose 2 pins for output; can be any valid output pins:
int dataPin  = 2;
int clockPin = 3;
int dataPin2 = 6;
int clockPin2 = 7;
int dataPin3 = 8;
int clockPin3 = 9;
int button1 = 4;
int button2 = 5;
int bubblePin = 10;

// First parameter is the number of LEDs in the strand.  The LED strips
// are 32 LEDs per meter but you can extend or cut the strip.  Next two
// parameters are SPI data and clock pins:
LPD8806 strip = LPD8806(nLEDs, dataPin, clockPin);
LPD8806 strip2 = LPD8806(nLEDs2, dataPin2, clockPin2);
LPD8806 strip3 = LPD8806(nLEDs3, dataPin3, clockPin3);

//Timing Parameters
int Button1_time = 0;
int Button2_time = 23000;


void setup() {
  pinMode(button1,INPUT_PULLUP);       // turn on pullup resistors.  https://www.arduino.cc/en/Tutorial/InputPullupSerial
  pinMode(button2,INPUT_PULLUP);       // turn on pullup resistors
  pinMode(bubblePin,OUTPUT);
  strip.begin();
  strip2.begin();
  strip3.begin();
  strip.show();
  strip2.show();
  strip3.show();
}

void loop() {
 if(digitalRead(button1)==HIGH)
 { 
 colorChase(strip.Color(0,127,127),(0,0,127),(127,0,127),50);
 Button1_time = millis();
 }
 if(digitalRead(button2)==LOW)
 { 
 colorChase2(strip2.Color(0,127,127),(0,0,127),(127,0,127),50); 
 Button2_time = millis();
 }
 if(abs(Button1_time-Button2_time)<=2500)
 {
  colorChase3(strip3.Color(0,127,127),(0,0,127),(127,0,127),50);
 }
}

// Chase one dot down the full strip.
void colorChase(uint32_t c,uint32_t c1,uint32_t c2, uint8_t wait) {
  int i;

  // Start by turning all pixels off:
  for(i=0; i<strip.numPixels(); i++) strip.setPixelColor(i, 0);

  for(i=0; i<strip.numPixels()/nLEDs; i++) {
  strip.setPixelColor(i, c); // Set new pixel 'on'
  strip.show();              // Refresh LED states
  strip.setPixelColor(i, 0); // Erase pixel, but don't refresh!
  delay(wait);
  }
  for(i=strip.numPixels()/nLEDs; i<2*strip.numPixels()/nLEDs; i++) {
  strip.setPixelColor(i, c1,i-nLEDs,c); // Set new pixel 'on'
  strip.show();              // Refresh LED states
  strip.setPixelColor(i, 0,i-nLEDs,0); // Erase pixel, but don't refresh!
  delay(wait);
  }
  for(i=2*strip.numPixels()/nLEDs; i<3*strip.numPixels()/nLEDs; i++) {
  strip.setPixelColor(i, c2,i-nLEDs,c1); // Set new pixel 'on'
  strip.setPixelColor(i-nLEDs*2,c2);
  strip.show();              // Refresh LED states
  strip.setPixelColor(i, 0,i-nLEDs,0); // Erase pixel, but don't refresh!
  strip.setPixelColor(i-nLEDs*2,0);
  delay(wait);
  }
    
  for(i=3*strip.numPixels()/nLEDs; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, c,i-1,c1); // Set new pixel 'on'
    strip.setPixelColor(i-2,c2);
    strip.show();              // Refresh LED states
    strip.setPixelColor(i, 0,i-1,0); // Erase pixel, but don't refresh!
    strip.setPixelColor(i-2, 0);
    delay(wait);
  }
  for(i=0;i<strip.numPixels();i++)
  { 
    strip.setPixelColor(i,0);
  }

  strip.show(); // Refresh to turn off last pixel
}

void colorChase2(uint32_t c,uint32_t c1,uint32_t c2, uint8_t wait) {
  int i;

  // Start by turning all pixels off:
  for(i=0; i<strip2.numPixels(); i++) strip2.setPixelColor(i, 0);

  for(i=0; i<strip.numPixels()/nLEDs2; i++) {
  strip2.setPixelColor(i, c); // Set new pixel 'on'
  strip2.show();              // Refresh LED states
  strip2.setPixelColor(i, 0); // Erase pixel, but don't refresh!
  delay(wait);
  }
  for(i=strip2.numPixels()/nLEDs2; i<2*strip2.numPixels()/nLEDs2; i++) {
  strip2.setPixelColor(i, c1,i-nLEDs2,c); // Set new pixel 'on'
  strip2.show();              // Refresh LED states
  strip2.setPixelColor(i, 0,i-nLEDs2,0); // Erase pixel, but don't refresh!
  delay(wait);
  }
  for(i=2*strip2.numPixels()/nLEDs2; i<3*strip2.numPixels()/nLEDs2; i++) {
  strip2.setPixelColor(i, c2,i-nLEDs2,c1); // Set new pixel 'on'
  strip2.setPixelColor(i-nLEDs2*2,c2);
  strip2.show();              // Refresh LED states
  strip2.setPixelColor(i, 0,i-nLEDs2,0); // Erase pixel, but don't refresh!
  strip2.setPixelColor(i-nLEDs2*2,0);
  delay(wait);
  }
    
  for(i=3*strip2.numPixels()/nLEDs2; i<strip2.numPixels(); i++) {
    strip2.setPixelColor(i, c,i-1,c1); // Set new pixel 'on'
    strip2.setPixelColor(i-2,c2);
    strip2.show();              // Refresh LED states
    strip2.setPixelColor(i, 0,i-1,0); // Erase pixel, but don't refresh!
    strip2.setPixelColor(i-2, 0);
    delay(wait);
  }
  for(i=0;i<strip2.numPixels();i++)
  { 
    strip2.setPixelColor(i,0);
  }

  strip2.show(); // Refresh to turn off last pixel
}

void colorChase3(uint32_t c,uint32_t c1,uint32_t c2, uint8_t wait) {
  int i;

  // Start by turning all pixels off:
  for(i=0; i<strip3.numPixels(); i++) strip3.setPixelColor(i, 0);

  for(i=0; i<strip3.numPixels()/nLEDs3; i++) {
  strip3.setPixelColor(i, c); // Set new pixel 'on'
  strip3.show();              // Refresh LED states
  strip3.setPixelColor(i, 0); // Erase pixel, but don't refresh!
  delay(wait);
  }
  for(i=strip3.numPixels()/nLEDs3; i<2*strip3.numPixels()/nLEDs3; i++) {
  strip3.setPixelColor(i, c1,i-nLEDs3,c); // Set new pixel 'on'
  strip3.show();              // Refresh LED states
  strip3.setPixelColor(i, 0,i-nLEDs3,0); // Erase pixel, but don't refresh!
  delay(wait);
  }
  for(i=2*strip3.numPixels()/nLEDs3; i<3*strip3.numPixels()/nLEDs3; i++) {
  strip3.setPixelColor(i, c2,i-nLEDs3,c1); // Set new pixel 'on'
  strip3.setPixelColor(i-nLEDs3*2,c2);
  strip3.show();              // Refresh LED states
  strip3.setPixelColor(i, 0,i-nLEDs3,0); // Erase pixel, but don't refresh!
  strip3.setPixelColor(i-nLEDs3*2,0);
  delay(wait);
  }
    
  for(i=3*strip3.numPixels()/nLEDs3; i<strip3.numPixels(); i++) {
    strip3.setPixelColor(i, c,i-1,c1); // Set new pixel 'on'
    strip3.setPixelColor(i-2,c2);
    strip3.show();              // Refresh LED states
    strip3.setPixelColor(i, 0,i-1,0); // Erase pixel, but don't refresh!
    strip3.setPixelColor(i-2, 0);
    delay(wait);
  }
  for(i=0;i<strip3.numPixels();i++)
  { 
    strip3.setPixelColor(i,0);
  }

  strip3.show(); // Refresh to turn off last pixel

  digitalWrite(bubblePin,HIGH);
  strip3.show(); 
  delay(3000);
  digitalWrite(bubblePin,LOW);

  Button1_time = 0;
  Button2_time = 23000;
}
